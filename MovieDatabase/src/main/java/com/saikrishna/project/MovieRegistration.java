package com.saikrishna.project;

import java.util.ArrayList;
import java.util.List;

public class MovieRegistration {
	
	private List<Movie> movieRecords;
	
	private static MovieRegistration mvregd = null;
	
	private MovieRegistration() {
		movieRecords = new ArrayList<Movie>();
	}
	
	public static MovieRegistration getInstance() {
		if(mvregd == null) {
			mvregd = new MovieRegistration();
			return mvregd;
		}
		else {
			return mvregd;
		}
	}
	
	public void add(Movie mv) {
		movieRecords.add(mv);
	}
	
	/****** Updating Movie Logic need to be developed *********/
	
	
	/****** Deleting Movie Logic need to be developed *********/
	
	public List<Movie> getMovieRecords() {
	    return movieRecords;
	    }

}
