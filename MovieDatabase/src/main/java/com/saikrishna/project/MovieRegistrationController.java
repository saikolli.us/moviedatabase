package com.saikrishna.project;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

public class MovieRegistrationController {
	
	@RequestMapping(method = RequestMethod.POST, value="/register/movie")
	
	@ResponseBody
	public MovieRegistrationReply registerMovie(@RequestBody Movie movie) {
		System.out.println("In Movie");
		MovieRegistrationReply mvregreply = new MovieRegistrationReply();
		MovieRegistration.getInstance().add(movie);
		mvregreply.setName(movie.getName());
		mvregreply.setYearOfRelease(movie.getYearOfRelease());
		mvregreply.setDirector(movie.getDirector());
		mvregreply.setDescription(movie.getDescription());
		
		return mvregreply;
		
	}
}
