package com.saikrishna.project;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MovieRetrieveController {
	@RequestMapping(method = RequestMethod.GET, value="/movie/allmovie")
	
	@ResponseBody
	public List<Movie> getAllMovies(){
		
		return MovieRegistration.getInstance().getMovieRecords();
	}
	
}
